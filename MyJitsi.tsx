import React from "react";
import {StyleSheet, View} from "react-native";
import WebView from "react-native-webview";
import * as Permissions from 'expo-permissions';
import { Dimensions } from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class MyJitsi extends React.Component{
    fn = async () => {
        await Permissions.askAsync(Permissions.CAMERA);
        await Permissions.askAsync(Permissions.AUDIO_RECORDING);
    };

    componentDidMount() {
        this.fn();
    }

    render() {
        return (
            <View>
                <WebView
                    userAgent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36"
                    originWhitelist={['*']}
                    source={{ uri: `https://meet.jit.si/Hello12312312313212` }}
                    style={styles.webview}
                    bounces={true}
                    allowsInlineMediaPlayback
                    mediaPlaybackRequiresUserAction={false}
                    startInLoadingState
                    scalesPageToFit
                    javaScriptEnabled={true}
                    javaScriptEnabledAndroid
                    useWebkit
                />
            </View>
        );
    }

}

const styles = StyleSheet.create({

    webview: {
        width: windowWidth,
        height: windowHeight-20,
        marginTop: 20,
        borderWidth: 1,
    }
});

export default MyJitsi;